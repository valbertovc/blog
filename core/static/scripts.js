$( document ).ready(function() {
  $('[data-toggle="tooltip"]').tooltip();

  $('i[data-toggle="collapse"]').click(function (e)   
    { $(this).toggleClass('fa-plus fa-minus'); 
  });

  $('i[data-animation]').hover(
    function(){ $(this).addClass('animated ' + $(this).data('animation')) }, 
    function(){ $(this).removeClass('animated ' + $(this).data('animation')) }
  );
});

function recommend_comment(data){
  $('#recommend_comment_'+data.pk).toggleClass('fa-star fa-star-o');
  $('#total_recommends_comment_'+data.pk).html(data.total);
}

function recommend_post(data){
  $('#recommend_post_'+data.pk).toggleClass('fa-star fa-star-o');
  $('#total_recommends_post_'+data.pk).html(data.total);
}

function like_comment(data){
  $('#like_comment_'+data.pk).toggleClass('fa-thumbs-up fa-thumbs-o-up');
  $('#total_like_comment_'+data.pk).html(data.total);
}

function unlike_comment(data){
  $('#unlike_comment_'+data.pk).toggleClass('fa-thumbs-down fa-thumbs-o-down');
  $('#total_unlike_comment_'+data.pk).html(data.total);
}


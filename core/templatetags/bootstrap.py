from django import template
register = template.Library()

def get_params(css):
    attrs = {}
    definition = css.split(',')
   
    for d in definition:
        if ':' not in d:
            attrs['class'] = d
        else:
            t, v = d.split(':')
            attrs[t] = v
    return attrs

@register.filter(name='attrs')
def attrs(field, css):
    attrs = get_params(css)
    return field.as_widget(attrs=attrs)
from django import template
from django.core.urlresolvers import reverse
import re

register = template.Library()

@register.filter
def active(request, url):
    path = reverse(url)
    return 'active' if path in request.path else ''
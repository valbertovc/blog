from django import template
register = template.Library()

@register.filter
def is_post_recommended(user, post):
    return user.userprofile.is_post_recommended(post)

@register.filter
def is_comment_recommended(user, comment):
    return user.userprofile.is_comment_recommended(comment)

@register.filter
def is_comment_liked(user, comment):
    return user.userprofile.is_comment_liked(comment)

@register.filter
def is_comment_unliked(user, comment):
    return user.userprofile.is_comment_unliked(comment)
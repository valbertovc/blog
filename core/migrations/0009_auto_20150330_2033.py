# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20150330_2022'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comment',
            old_name='like',
            new_name='likes',
        ),
        migrations.RenameField(
            model_name='comment',
            old_name='unlike',
            new_name='unlikes',
        ),
    ]

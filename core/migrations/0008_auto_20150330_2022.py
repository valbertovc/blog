# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0007_auto_20150320_2156'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='like',
            field=models.ManyToManyField(related_query_name='comment_liked', related_name='comments_liked', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='unlike',
            field=models.ManyToManyField(related_query_name='comment_unliked', related_name='comments_unliked', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]

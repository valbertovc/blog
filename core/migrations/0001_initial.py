# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('color', models.CharField(max_length=15, choices=[('success', 'Verde'), ('info', 'Azul'), ('default', 'Cinza'), ('danger', 'Vermelho'), ('warning', 'Amarelo')])),
            ],
            options={
                'verbose_name': 'Categoria',
                'verbose_name_plural': 'Categorias',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(verbose_name='coment\xe1rio')),
                ('approved', models.BooleanField(default=False, verbose_name='aprovado')),
                ('published_at', models.DateTimeField(auto_now_add=True, verbose_name='publica\xe7\xe3o')),
            ],
            options={
                'verbose_name': 'Coment\xe1rio',
                'verbose_name_plural': 'Coment\xe1rios',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='t\xedtulo')),
                ('content', models.TextField(verbose_name='conte\xfado')),
                ('published_at', models.DateTimeField(verbose_name='publica\xe7\xe3o')),
                ('category', models.ForeignKey(related_query_name='post', related_name='posts', to='core.Category')),
                ('publisher', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Artigo',
                'verbose_name_plural': 'Artigos',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='comment',
            name='post',
            field=models.ForeignKey(related_query_name='coment', related_name='comments', verbose_name='post', to='core.Post'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='publisher',
            field=models.ForeignKey(verbose_name='leitor', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]

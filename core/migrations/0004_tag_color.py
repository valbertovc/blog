# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150301_2019'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='color',
            field=models.CharField(default='success', max_length=15, choices=[('success', 'Verde'), ('info', 'Azul'), ('default', 'Cinza'), ('danger', 'Vermelho'), ('warning', 'Amarelo')]),
            preserve_default=False,
        ),
    ]

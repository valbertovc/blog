# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0006_auto_20150306_2203'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='recommends',
            field=models.ManyToManyField(related_query_name='comment_recommended', related_name='comments_recommended', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='recommends',
            field=models.ManyToManyField(related_query_name='post_recommended', related_name='posts_recommended', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]

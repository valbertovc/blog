# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.shortcuts import redirect, render_to_response, get_object_or_404
from django.shortcuts import render_to_response
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.template import RequestContext

from core.models import Category, Post, Tag
from core.forms import PostForm, CommentForm, ContactForm

class ContactView(FormView):
    template_name = 'core/contact.html'
    form_class = ContactForm
    success_url = '/blog/contact'

    def form_valid(self, form):
        form.send_email()
        return super(ContactView, self).form_valid(form)

class PostListView(ListView):

    model = Post
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        context['category_list'] = Category.objects.all()
        context['tag_list'] = Tag.objects.all()
        return context

def about(request):
  template='core/about.html'
  username = settings.BLOG_OWNER_USER_NAME
  context = {'owner': User.objects.get(username=username)}
  return render_to_response(template, context,
                            context_instance=RequestContext(request))


@user_passes_test(lambda u: u.is_superuser)
def add_post(request):
    form = PostForm(request.POST or None)
    if form.is_valid():
        post = form.save(commit=False)
        post.author = request.user
        post.save()
        return redirect(post)
    return render_to_response('core/add_post.html', 
                              { 'form': form },
                              context_instance=RequestContext(request))

def view_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    form = CommentForm(request.POST or None)
    recommend_post_form = CommentForm(request.POST or None)
    if form.is_valid():
        comment = form.save(commit=False)
        comment.publisher = request.user
        comment.post = post
        comment.save()
        return redirect(request.path)
    form.initial['publisher'] = request.user.pk
    return render_to_response('core/post_detail.html',
                              {
                                  'post': post,
                                  'form': form,
                              },
                              context_instance=RequestContext(request))
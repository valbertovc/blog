# -*- coding:utf8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from core.models import Post
from core.models import Category
from core.models import Comment
from core.models import Tag

admin.site.register(Post)
admin.site.register(Category) 
admin.site.register(Comment)
admin.site.register(Tag) 

# -*- coding:utf8 -*-
from __future__ import unicode_literals
from django import forms
from django.core.mail import BadHeaderError
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from core.models import Post, Comment

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        exclude = ['published_at', 'publisher']

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']

class ContactForm(forms.Form):
    name = forms.CharField(help_text="Informe seu nome completo")
    email = forms.EmailField(help_text="Seu e-mail para contato")
    topic = forms.CharField(help_text="Assunto que deseja tratar")
    message = forms.CharField(widget=forms.Textarea)

    def send_email(self):
        name = self.cleaned_data.get('name', '')
        subject = self.cleaned_data.get('topic', '')
        message = self.cleaned_data.get('message', '')
        from_email = self.cleaned_data.get('email', '')

        if subject and message and from_email:
            try:
                print "email enviado"
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
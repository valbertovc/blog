import json
from dajaxice.decorators import dajaxice_register
from core.models import Post, Comment

@dajaxice_register
def recommend_post(request, pk):
    post = Post.objects.get(pk=pk)
    user = request.user
    
    if user.userprofile.is_post_recommended(post):
        user.posts_recommended.remove(post)
        user.save()

    else:
        user.posts_recommended.add(post)
        user.save()
        
    total = str(len(post.recommends.all()))

    return json.dumps({'total': total, 'pk': post.pk})

@dajaxice_register
def recommend_comment(request, pk):
    comment = Comment.objects.get(pk=pk)
    user = request.user
    
    if user.userprofile.is_comment_recommended(comment):
        user.comments_recommended.remove(comment)
        user.save()

    else:
        user.comments_recommended.add(comment)
        user.save()
    
    total = str(len(comment.recommends.all()))

    return json.dumps({'total': total, 'pk': comment.pk})

@dajaxice_register
def like_comment(request, pk):
    comment = Comment.objects.get(pk=pk)
    user = request.user
    
    if user.userprofile.is_comment_liked(comment):
        user.comments_liked.remove(comment)
        user.save()

    else:
        user.comments_liked.add(comment)
        user.save()
    
    total = str(len(comment.likes.all()))

    return json.dumps({'total': total, 'pk': comment.pk})

@dajaxice_register
def unlike_comment(request, pk):
    comment = Comment.objects.get(pk=pk)
    user = request.user
    
    if user.userprofile.is_comment_unliked(comment):
        user.comments_unliked.remove(comment)
        user.save()

    else:
        user.comments_unliked.add(comment)
        user.save()
    
    total = str(len(comment.unlikes.all()))

    return json.dumps({'total': total, 'pk': comment.pk})
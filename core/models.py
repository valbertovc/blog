# -*- coding:utf8 -*-
from __future__ import unicode_literals 
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from ckeditor.fields import RichTextField

COR_CHOICES = (
	('success', 'Verde'),
	('info', 'Azul'),
	('default', 'Cinza'),
	('danger', 'Vermelho'),
	('warning', 'Amarelo'),
)

class Category(models.Model):
	class Meta:
		verbose_name = 'Categoria'
		verbose_name_plural = 'Categorias'

	name = models.CharField(max_length=255)
	color = models.CharField(max_length=15, choices=COR_CHOICES)

	def __unicode__(self):
		return self.name

	def percent_representation(self):
		total = float(Post.objects.all().count())
		num = float(self.posts.count())
		if total == 0.0:
			return 0.0
		else:
			return ((num/total) * 100)
		 
	def get_absolute_url(self):
		return reverse('category-detail', kwargs={'slug': self.name})

class Tag(models.Model):
	name = models.TextField(verbose_name='Nome', max_length=50)
	color = models.CharField(max_length=15, choices=COR_CHOICES)

	def __unicode__(self):
		return self.name

	def percent_representation(self):
		total = float(Post.objects.all().count())
		num = float(self.posts.count())
		if total == 0.0:
			return 0.0
		else:
			return ((num/total) * 100)

	def get_absolute_url(self):
		return reverse('tag-detail', kwargs={'slug': self.name})

class Post(models.Model):
	class Meta:
		verbose_name = 'Artigo'
		verbose_name_plural = 'Artigos'

	title = models.CharField(max_length=100, verbose_name='título')
	content = RichTextField(verbose_name='conteúdo')
	published_at = models.DateTimeField(verbose_name='publicação', auto_now_add=True)
	category = models.ForeignKey(Category, related_name="posts", related_query_name="post")
	publisher = models.ForeignKey(User, related_name="posts", related_query_name="post")
	tags = models.ManyToManyField(Tag, related_name="posts", related_query_name="post")
	recommends = models.ManyToManyField(User, related_name='posts_recommended', related_query_name='post_recommended')

	def __unicode__(self):
		return "#{} - {} by {}".format(self.pk, self.title, self.publisher)

	def resume(self):
		return " ".join(self.content.split()[:60]) + '...'

	def get_absolute_url(self):
		return reverse('post-detail', kwargs={'pk': self.pk})

class Comment(models.Model):
	class Meta:
		verbose_name = 'Comentário'
		verbose_name_plural = 'Comentários'

	text = models.TextField(verbose_name='deixe um comentário')
	approved = models.BooleanField(default=False, verbose_name='aprovado')
	publisher = models.ForeignKey(User, verbose_name='leitor')
	published_at = models.DateTimeField(verbose_name='publicação', auto_now_add=True)
	post = models.ForeignKey(Post, verbose_name='post', related_name="comments", related_query_name="coment")
	recommends = models.ManyToManyField(User, related_name='comments_recommended', related_query_name='comment_recommended')
	likes = models.ManyToManyField(User, related_name='comments_liked', related_query_name='comment_liked')
	unlikes = models.ManyToManyField(User, related_name='comments_unliked', related_query_name='comment_unliked')
	
	def __unicode__(self):
		return '{}... by {} em {}'.format(self.texto[:15], self.owner, self.publicacao)
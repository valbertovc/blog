# Blog em Django + JQuery  + Bootstrap + Font-awesome #

Este projeto foi construído apenas com intuito de aprendizado. O objetivo dele foi aprender sobre a tecnologia Python e Django. Além disso, aproveitei a oportunidade para adicionar misturar outros recursos. Versão: 0.1.0

## Conteúdo ##

* Tecnologias utilizadas
* Funcionalidades
* Recursos do Django

## Tecnologias utilizadas ##

* Linguagem de programação: Python 2.7.6
* Desenvolvimento web: Django 1.7.0
* JavaScript: JQuery 1.11.2
* Icones: Font-awesome 4.3.0
* Animações: animate.css
* Front-end: Bootstrap 3.3.2
* WYSIWYG editor: tinymce 4.1
* Ajax: Dajaxice 0.7

## Funcionalidades ##

##Recursos do Django##
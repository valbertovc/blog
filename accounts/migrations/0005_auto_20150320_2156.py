# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20150306_2227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='social',
            name='user',
            field=models.ForeignKey(related_name='social_set', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='socialnetwork',
            name='icon',
            field=models.CharField(max_length=30, choices=[(b'xing-square', b'xing-square'), (b'reddit-square', b'reddit-square'), (b'google-plus', b'google-plus'), (b'spotify', b'spotify'), (b'facebook-square', b'facebook-square'), (b'stumbleupon-circle', b'stumbleupon-circle'), (b'twitter-square', b'twitter-square'), (b'foursquare', b'foursquare'), (b'adn', b'adn'), (b'deviantart', b'deviantart'), (b'css3', b'css3'), (b'skyatlas', b'skyatlas'), (b'weixin', b'weixin'), (b'stack-overflow', b'stack-overflow'), (b'facebook', b'facebook'), (b'facebook-f (alias)', b'facebook-f (alias)'), (b'qq', b'qq'), (b'leanpub', b'leanpub'), (b'youtube-play', b'youtube-play'), (b'pied-piper', b'pied-piper'), (b'steam', b'steam'), (b'google', b'google'), (b'instagram', b'instagram'), (b'twitter', b'twitter'), (b'vine', b'vine'), (b'tumblr', b'tumblr'), (b'maxcdn', b'maxcdn'), (b'btc', b'btc'), (b'lastfm-square', b'lastfm-square'), (b'empire', b'empire'), (b'weibo', b'weibo'), (b'pied-piper-alt', b'pied-piper-alt'), (b'xing', b'xing'), (b'pinterest', b'pinterest'), (b'facebook-official', b'facebook-official'), (b'flickr', b'flickr'), (b'cc-amex', b'cc-amex'), (b'reddit', b'reddit'), (b'tumblr-square', b'tumblr-square'), (b'renren', b'renren'), (b'pinterest-p', b'pinterest-p'), (b'vimeo-square', b'vimeo-square'), (b'rebel', b'rebel'), (b'github-square', b'github-square'), (b'soundcloud', b'soundcloud'), (b'sellsy', b'sellsy'), (b'drupal', b'drupal'), (b'cc-mastercard', b'cc-mastercard'), (b'buysellads', b'buysellads'), (b'pinterest-square', b'pinterest-square'), (b'ge (alias)', b'ge (alias)'), (b'twitch', b'twitch'), (b'wechat (alias)', b'wechat (alias)'), (b'html5', b'html5'), (b'apple', b'apple'), (b'gittip (alias)', b'gittip (alias)'), (b'cc-stripe', b'cc-stripe'), (b'linkedin-square', b'linkedin-square'), (b'delicious', b'delicious'), (b'linux', b'linux'), (b'dribbble', b'dribbble'), (b'jsfiddle', b'jsfiddle'), (b'tencent-weibo', b'tencent-weibo'), (b'git', b'git'), (b'connectdevelop', b'connectdevelop'), (b'behance', b'behance'), (b'steam-square', b'steam-square'), (b'codepen', b'codepen'), (b'wordpress', b'wordpress'), (b'medium', b'medium'), (b'bitbucket-square', b'bitbucket-square'), (b'paypal', b'paypal'), (b'yahoo', b'yahoo'), (b'share-alt', b'share-alt'), (b'shirtsinbulk', b'shirtsinbulk'), (b'viacoin', b'viacoin'), (b'digg', b'digg'), (b'whatsapp', b'whatsapp'), (b'angellist', b'angellist'), (b'stumbleupon', b'stumbleupon'), (b'bitcoin (alias)', b'bitcoin (alias)'), (b'slack', b'slack'), (b'simplybuilt', b'simplybuilt'), (b'cc-paypal', b'cc-paypal'), (b'trello', b'trello'), (b'meanpath', b'meanpath'), (b'git-square', b'git-square'), (b'vk', b'vk'), (b'linkedin', b'linkedin'), (b'share-alt-square', b'share-alt-square'), (b'skype', b'skype'), (b'slideshare', b'slideshare'), (b'dashcube', b'dashcube'), (b'joomla', b'joomla'), (b'google-wallet', b'google-wallet'), (b'github-alt', b'github-alt'), (b'lastfm', b'lastfm'), (b'android', b'android'), (b'pagelines', b'pagelines'), (b'stack-exchange', b'stack-exchange'), (b'openid', b'openid'), (b'ra (alias)', b'ra (alias)'), (b'gratipay', b'gratipay'), (b'google-plus-square', b'google-plus-square'), (b'dropbox', b'dropbox'), (b'ioxhost', b'ioxhost'), (b'forumbee', b'forumbee'), (b'bitbucket', b'bitbucket'), (b'github', b'github'), (b'yelp', b'yelp'), (b'windows', b'windows'), (b'behance-square', b'behance-square'), (b'youtube', b'youtube'), (b'hacker-news', b'hacker-news'), (b'cc-visa', b'cc-visa'), (b'cc-discover', b'cc-discover'), (b'youtube-square', b'youtube-square')]),
            preserve_default=True,
        ),
    ]

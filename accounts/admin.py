# -*- coding:utf8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.auth.models import User

from accounts.models import UserProfile
from accounts.models import Social
from accounts.models import SocialNetwork

class UserSocialInline(admin.TabularInline):
    model = Social

class UserProfileInline(admin.TabularInline):
    model = UserProfile
    inlines = (UserSocialInline,)

class UserAdmin(DjangoUserAdmin):
    inlines = (UserProfileInline, UserSocialInline)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(SocialNetwork)




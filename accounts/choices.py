SOCIAL_CHOICES = {
  'adn': 'adn',
  'android': 'android',
  'angellist': 'angellist',
  'apple': 'apple',
  'behance': 'behance',
  'behance-square' : 'behance-square',
  'bitbucket': 'bitbucket',
  'bitbucket-square' : 'bitbucket-square',
  'bitcoin (alias)': 'bitcoin (alias)',
  'btc': 'btc',
  'buysellads' : 'buysellads',
  'cc-amex': 'cc-amex',
  'cc-discover': 'cc-discover',
  'cc-mastercard': 'cc-mastercard',
  'cc-paypal': 'cc-paypal',
  'cc-stripe': 'cc-stripe',
  'cc-visa': 'cc-visa',
  'codepen': 'codepen',
  'connectdevelop' : 'connectdevelop',
  'css3' : 'css3',
  'dashcube' : 'dashcube',
  'delicious': 'delicious',
  'deviantart' : 'deviantart',
  'digg' : 'digg',
  'dribbble' : 'dribbble',
  'dropbox': 'dropbox',
  'drupal' : 'drupal',
  'empire' : 'empire',
  'facebook' : 'facebook',
  'facebook-f (alias)' : 'facebook-f (alias)',
  'facebook-official': 'facebook-official',
  'facebook-square': 'facebook-square',
  'flickr' : 'flickr',
  'forumbee' : 'forumbee',
  'foursquare' : 'foursquare',
  'ge (alias)' : 'ge (alias)',
  'git': 'git',
  'git-square' : 'git-square',
  'github' : 'github',
  'github-alt' : 'github-alt',
  'github-square': 'github-square',
  'gittip (alias)' : 'gittip (alias)',
  'google' : 'google',
  'google-plus': 'google-plus',
  'google-plus-square' : 'google-plus-square',
  'google-wallet': 'google-wallet',
  'gratipay' : 'gratipay',
  'hacker-news': 'hacker-news',
  'html5': 'html5',
  'instagram': 'instagram',
  'ioxhost': 'ioxhost',
  'joomla' : 'joomla',
  'jsfiddle' : 'jsfiddle',
  'lastfm' : 'lastfm',
  'lastfm-square': 'lastfm-square',
  'leanpub': 'leanpub',
  'linkedin' : 'linkedin',
  'linkedin-square': 'linkedin-square',
  'linux': 'linux',
  'maxcdn' : 'maxcdn',
  'meanpath' : 'meanpath',
  'medium' : 'medium',
  'openid' : 'openid',
  'pagelines': 'pagelines',
  'paypal' : 'paypal',
  'pied-piper' : 'pied-piper',
  'pied-piper-alt' : 'pied-piper-alt',
  'pinterest': 'pinterest',
  'pinterest-p': 'pinterest-p',
  'pinterest-square' : 'pinterest-square',
  'qq' : 'qq',
  'ra (alias)' : 'ra (alias)',
  'rebel': 'rebel',
  'reddit' : 'reddit',
  'reddit-square': 'reddit-square',
  'renren' : 'renren',
  'sellsy' : 'sellsy',
  'share-alt': 'share-alt',
  'share-alt-square' : 'share-alt-square',
  'shirtsinbulk' : 'shirtsinbulk',
  'simplybuilt': 'simplybuilt',
  'skyatlas' : 'skyatlas',
  'skype': 'skype',
  'slack': 'slack',
  'slideshare' : 'slideshare',
  'soundcloud' : 'soundcloud',
  'spotify': 'spotify',
  'stack-exchange' : 'stack-exchange',
  'stack-overflow' : 'stack-overflow',
  'steam': 'steam',
  'steam-square' : 'steam-square',
  'stumbleupon': 'stumbleupon',
  'stumbleupon-circle' : 'stumbleupon-circle',
  'tencent-weibo': 'tencent-weibo',
  'trello' : 'trello',
  'tumblr' : 'tumblr',
  'tumblr-square': 'tumblr-square',
  'twitch' : 'twitch',
  'twitter': 'twitter',
  'twitter-square' : 'twitter-square',
  'viacoin': 'viacoin',
  'vimeo-square' : 'vimeo-square',
  'vine' : 'vine',
  'vk' : 'vk',
  'wechat (alias)' : 'wechat (alias)',
  'weibo': 'weibo',
  'weixin' : 'weixin',
  'whatsapp' : 'whatsapp',
  'windows': 'windows',
  'wordpress': 'wordpress',
  'xing' : 'xing',
  'xing-square': 'xing-square',
  'yahoo': 'yahoo',
  'yelp' : 'yelp',
  'youtube': 'youtube',
  'youtube-play' : 'youtube-play',
  'youtube-square' : 'youtube-square',
}
# -*- coding:utf8 -*-
from __future__ import unicode_literals
from django.db import models
from django.db.models import signals
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from accounts.choices import SOCIAL_CHOICES

class UserProfile(models.Model):  
    user = models.OneToOneField('auth.User')
    site = models.URLField()  
    bio = models.TextField()
    picture = models.ImageField(upload_to='profiles', blank=True)

    def __unicode__(self):
        return u'Profile of user: %s' % self.user.username

    def get_absolute_url(self):
		return reverse('profile-detail', kwargs={'slug': self.user.username})

    def is_post_recommended(self, post):
        ids = []
        for post_recommended in self.user.posts_recommended.all():
            ids.append(post_recommended.pk)
        return post.pk in ids

    def is_comment_recommended(self, comment):
        ids = []
        for comment_recommended in self.user.comments_recommended.all():
            ids.append(comment_recommended.pk)
        return comment.pk in ids

    def is_comment_liked(self, comment):
        ids = []
        for comment_liked in self.user.comments_liked.all():
            ids.append(comment_liked.pk)
        return comment.pk in ids

    def is_comment_unliked(self, comment):
        ids = []
        for comment_unliked in self.user.comments_unliked.all():
            ids.append(comment_unliked.pk)
        return comment.pk in ids

#faz com que, todo usuário tenha um profile
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

signals.post_save.connect(create_user_profile, sender=User)

class SocialNetwork(models.Model):
    icon = models.CharField(max_length=30, choices=SOCIAL_CHOICES.items())
    name = models.CharField(max_length=50)
    url = models.URLField()
    users = models.ManyToManyField(User, through='Social', 
                                        through_fields=('social_network', 'user'))

    def __unicode__(self):
        return self.name

class Social(models.Model):
    profile = models.CharField(max_length=100)
    social_network = models.ForeignKey(SocialNetwork)
    user = models.ForeignKey(User, related_name='social_set')

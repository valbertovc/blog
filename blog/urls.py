# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from core.views import PostListView, ContactView
from core.models import Post
from core.models import Category
from core.models import Tag
from accounts.models import UserProfile
from django.views.generic import TemplateView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

#Import dajaxice modules
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    #Rich textarea editor
    (r'^ckeditor/', include('ckeditor_uploader.urls')),

    #About page
    url(r'^blog/about$', 'core.views.about', name='about'),

    #Contact page
    url(r'^blog/contact$', ContactView.as_view(), name='contact'),

    #Posts page (home)
    url(r'^blog/$', PostListView.as_view(), name='post-list'),
    url(r'^blog/(?P<pk>[0-9]+)/$', 'core.views.view_post', name='post-detail'),
    url(r'^blog/category/$', ListView.as_view(model=Category), name='category-list'),
    url(r'^blog/category/(?P<slug>[\w -.?]+)/*$', DetailView.as_view(model=Category, slug_field='name', context_object_name='category'), name='category-detail'),

    url(r'^accounts/profile/(?P<slug>[\w-]+)/$', DetailView.as_view(model=UserProfile, slug_field='user__username', context_object_name='profile'), name='profile-detail'),

    url(r'^blog/tag/(?P<slug>[\w -.?]+)/$', DetailView.as_view(model=Tag, slug_field='name', context_object_name='tag'), name='tag-detail'),
    url(r'^blog/tag/$', ListView.as_view(model=Tag), name='tag-list'),

    #Configure Dajaxice urls
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += staticfiles_urlpatterns()
